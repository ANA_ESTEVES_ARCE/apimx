var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/aesteves/collections"
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMlabRaiz;
var urlClientes = "https://api.mlab.com/api/1/databases/aesteves/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"

var clienteMlab = requestjson.createClient(urlClientes)

app.listen(port);

var bodyparser = require('body-parser');
app.use(bodyparser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Request-Width, Content-Type, Accept");
  next();
})

var movimientosJSON = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req,res){
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function(req,res){
  res.send("Hemos recibido su petición Post cambiada");
})

app.put('/', function(req,res){
  res.send("Hemos recibido su petición put NodeJS");
})

app.delete('/', function(req,res){
  res.send("Hemos recibido su petición delete NodeJS");
})

app.get('/Clientes/:idcliente', function(req,res){
  res.send("Aqui tiene al cliente número " + req.params.idcliente);
})

app.get('/v1/Movimientos', function(req,res){
  res.sendfile('movimientosv1.json');
})

app.get('/v2/Movimientos', function(req,res){
  res.json(movimientosJSON);
})

app.get('v2/Movimientos/:id', function(req,res){
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id]);
})

app.get('v2/MovimientosQuery', function(req,res){
  console.log(req.query);
  res.send("recibido");
})

app.post('/v2/Movimientos', function(req, res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length +1
  movimientosJSON.push(nuevo)
  res.send("Movimiento dado de alta")
})

app.get('/Clientes', function(req,res){
  clienteMlab.get('',function(err, resM, body){
    if (err){
      console.log(body)
    }else {
      res.send(body);
    }
  })
})

app.post('/Clientes', function(req,res){
  clienteMlab.post('', req.body, function(err, resM, body){
      res.send(body);
  })
})

app.post('/Login',function(req,res){
  res.set("Access-Control-Allow-Headers", "Content-Type")
  var email = req.body.email
  var password = req.body.password

  var query = 'q={"email":"'+ email +'","password":"'+ password +'"}'
  console.log(query)

  var urlMlab = urlMlabRaiz + "/Usuarios?" + query + "&" + apiKey;
  console.log(urlMlab)
  clienteMlabRaiz = requestjson.createClient(urlMlab)

  clienteMlabRaiz.get('', function(err,resM, body){
  if (!err){
    if (body.length == 1) { //login ok, se encontro 1 docto
      res.status(200).send('Usuario Logeado')
    } else {
      res.status(404).send('Usuario no encontrado')
      }
    }
  })
})
